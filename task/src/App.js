import React, { Component } from 'react';
import './App.css';
import Modal from "./Components/UI/Modal/Modal";
import Alert from "./Components/UI/Alert/Alert";

class App extends Component {

  state = {
    modal: false,
  };

  continued = () => {
    alert('You clicked continued');
  };

  closed = () => {
    alert('You clicked closed');
  };

  configurationButton = [
    {type: 'primary', label: 'Continue', clicked: this.continued},
    {type: 'danger', label: 'Close', clicked: this.closed},
  ];


  modalShow = () => {
    this.setState({modal: true});
  };

  modalHide = () => {
    this.setState({modal: false});
  };

  closeHandler = () => {
    alert('You clicked close');
  };

  render() {
    return (
      <div className="App">
        <button className='ModalShow'
                onClick={this.modalShow}>Modal show
        </button>
        <Modal show={this.state.modal}
               closed={this.modalHide}
               title="Title of modal"
               button={this.configurationButton}>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem impedit, velit. Architecto officia quidem reiciendis.</p>
        </Modal>

        <Alert type='success'>
          <p>This is a success type alert</p>
        </Alert>
        <Alert type='primary'>
          <p>This is a primary type alert</p>
        </Alert>
        <Alert type='danger'>
          <p>This is a danger type alert</p>
        </Alert>
        <Alert type='warning'>
          <p>This is a warning type alert</p>
        </Alert>
        <Alert type='warning'>
          <p>This is a warning type alert</p>
          <p>And i don't transfer "CloseHandler"</p>
        </Alert>
        <Alert type='warning'
               dismiss={this.closeHandler}>
          <p>This is a warning type alert</p>
        </Alert>
        <Alert type='warning'
               dismiss={this.closeHandler}
               clickDismissable>
          <p>This is a warning type alert</p>
        </Alert>
      </div>
    );
  }
}

export default App;
