import React from 'react';
import './Modal.css';
import Wrapper from "../../../hoc/Wrapper";

const Modal = props => (
  <Wrapper>
    <div className='Modal'
         style={{
           transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
           opacity : props.show ? '1' : '0'}}>
      {props.title}
      {props.children}
      {props.button.map((config, index) => {
        return (<button className={config.type}
                        style={{marginRight: '15px', cursor: 'pointer'}}
                        onClick={config.clicked}
                        key={index}>{config.label}</button>);

      })}
    </div>
  </Wrapper>
);

export default Modal;