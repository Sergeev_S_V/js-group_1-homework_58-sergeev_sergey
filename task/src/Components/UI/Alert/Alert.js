import React from 'react';
import './Alert.css';
import Wrapper from "../../../hoc/Wrapper";

const Alert = (props) => {
  return (
    <Wrapper>
      <div className={props.type}
           onClick={props.clickDismissable ? props.dismiss : null}>
        {props.clickDismissable || !props.dismiss ? null
        : <button className='btn' onClick={props.dismiss}>X</button>}
        {props.children}
      </div>
    </Wrapper>
  );
};

export default Alert;